### Это ...
<!-- Чтобы выбрать пункт, замените [] на [x] без пробелов вокруг x -->
- [ ] Bug fix
- [ ] Новая фича
- [ ] Расширение функциональности
- [ ] Рефакторинг кода
- [ ] Новые тесты
- [ ] Обновление документации
- [ ] Обновление стилей
- [ ] Другое

### :link: Ссылки на связанные ресурсы
<!-- Список ссылок на документацию, задачу в джире и т.п. -->

### :bulb: Решение
<!-- Описание проблемы и ее решение. Можно приложить скриншот, если проблема связана с UI -->

### :book: Changelog
<!-- Краткое резюме об внесенных изменениях -->
